using System.Windows;
using Autofac;
using JustSurvey.Models;
using MugenMvvmToolkit;
using MugenMvvmToolkit.WPF.Infrastructure;

namespace JustSurvey
{
    public partial class App : Application
    {
        public App() {
            var builder = new ContainerBuilder();
            builder.Register(context => new SurveyDataProvider()).AsImplementedInterfaces();
            
            // ReSharper disable once ObjectCreationAsStatement
            new Bootstrapper<JustSurveyApplication>(this, new AutofacContainer(builder));
        }
    }
}