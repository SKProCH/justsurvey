﻿using System.Windows.Controls;
using MaterialDesignThemes.Wpf;
using MugenMvvmToolkit.Models;

namespace JustSurvey.Views {
    public partial class EditDialog : UserControl {
        public bool CanCancel { get; set; }
        public bool AcceptsReturns { get; private set; }
        public string Text { get; set; }

        public EditDialog(bool acceptsReturns, string text = "") : this(acceptsReturns, false, text) { }

        public EditDialog(bool acceptsReturns, bool canCancel = false, string text = "") {
            CanCancel = canCancel;
            Text = text;
            AcceptsReturns = acceptsReturns;
            Cancel = new RelayCommand(o => { DialogHost.CloseDialogCommand.Execute(null, null); });
            Continue = new RelayCommand(o => { DialogHost.CloseDialogCommand.Execute(Text, null); });
            InitializeComponent();
            DataContext = this;
        }

        public RelayCommand Continue { get; set; }

        public RelayCommand Cancel { get; set; }
    }
}