﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using JustSurvey.Models;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using MugenMvvmToolkit.Models;
using MugenMvvmToolkit.ViewModels;

namespace JustSurvey.ViewModels {
    public class SurveyVm : ValidatableViewModel {
        public SnackbarMessageQueue MessageQueue { get; set; } = new SnackbarMessageQueue(TimeSpan.FromSeconds(4)) {IgnoreDuplicate = false};

        public SurveyVm(IDataProvider<SurveyData> dataProvider) {
            Exit = new RelayCommand(o => { CloseCommand.Execute(null); });
            ContinueCommand = new RelayCommand(async o => {
                ContinueStarted = true;
                if (CurrentIndex + 1 < Data.Count) {
                    CurrentIndex++;
                    Answer = "";
                    Percentage = (CurrentIndex + 0.5) / Data.Count * 100;
                }
                else {
                    Percentage = 100;
                    var emailSenderService = new EmailSenderService();
                    StringBuilder builder = new StringBuilder();
                    foreach (var answerData in Data) {
                        builder.AppendLine($"Q: {answerData.Question}\nA: {answerData.Answer}\n\n");
                    }

                    try {
                        await emailSenderService.Send(builder.ToString(), Email);
                        CloseCommand.Execute(null);
                    }
                    catch (Exception e) {
                        MessageQueue.Enqueue(e.Message);
                        CanExit = true;
                    }

                    var registryKey = Registry.CurrentUser.CreateSubKey("SOFTWARE")?.CreateSubKey("JustSurvey");
                    registryKey?.SetValue(Email, 1);
                    registryKey?.Close();
                    ContinueStarted = false;
                }
            }, o => !ContinueStarted);

            var surveyData = dataProvider.Provide();
            Data = new ObservableCollection<AnswerData>(surveyData.Questions.Select(s => new AnswerData(s)));
            Email = surveyData.TargetEmail;
        }

        public bool ContinueStarted { get; set; }

        public bool CanExit { get; set; }

        public ObservableCollection<AnswerData> Data { get; set; }

        public string Email { get; set; }

        public int CurrentIndex { get; set; }

        public string Answer {
            get => Data[CurrentIndex].Answer;
            set => Data[CurrentIndex].Answer = value;
        }

        public double Percentage { get; set; }

        public RelayCommand ContinueCommand { get; set; }
        public RelayCommand Exit { get; set; }
    }
}