﻿using JustSurvey.Models;
using MugenMvvmToolkit;
using MugenMvvmToolkit.Models;
using MugenMvvmToolkit.ViewModels;

namespace JustSurvey.ViewModels {
    public class MainVm : CloseableViewModel {
        private SurveyData? _surveyData;

        public MainVm(IDataProvider<SurveyData> dataProvider) {
            _surveyData = dataProvider.Provide();
            CanTakeASurvey = _surveyData != null;
        }

        public bool CanTakeASurvey { get; set; }

        public RelayCommand CreateCommand => new RelayCommand(() => {
            using (var myVm = GetViewModel<CreateVm>()) {
                myVm.ShowAsync();
            }
        });

        public RelayCommand OpenCommand => new RelayCommand(async () => {
            using (var surveyVm = GetViewModel<SurveyVm>()) {
                CloseCommand.Execute(null);
                await surveyVm.ShowAsync();
            }
        });
    }
}