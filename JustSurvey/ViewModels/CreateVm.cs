﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using JustSurvey.Models;
using JustSurvey.Views;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using MugenMvvmToolkit.Models;
using MugenMvvmToolkit.ViewModels;
using Newtonsoft.Json;
using PropertyChanged;

namespace JustSurvey.ViewModels {
    public class CreateVm : ValidatableViewModel {
        public static Regex EmailRegex =
            new Regex(
                @"^(([^<>()[\]\\.,;:\s@\""]+(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$");

        private string _email = "";

        public CreateVm() {
            Remove = new RelayCommand(o => { Questions.RemoveAt((int) o); });
            Edit = new RelayCommand(async o => {
                var old = Questions[(int) o];
                var result = (string) await DialogHost.Show(new EditDialog(false, true, old), "Create");
                if (result != null) {
                    Questions[(int) o] = result;
                }
            });
            Add = new RelayCommand(async o => {
                var result = (string) await DialogHost.Show(new EditDialog(false, true), "Create");
                if (result != null) {
                    Questions.Add(result);
                }
            });
            ContinueCommand = new RelayCommand(async o => {
                // Checking properties
                Email = _email;
                QuestionsCount = Questions.Count;
                if (Validator.HasErrors) return;

                var saveFileDialog = new SaveFileDialog {FileName = "JustSurvey.exe", DefaultExt = "exe"};
                if (saveFileDialog.ShowDialog() == true) {
                    try {
                        var destFileName = saveFileDialog.FileNames[0];
                        File.Copy(Environment.GetCommandLineArgs()[0], destFileName, true);
                        var surveyData = new SurveyData(Email) {Questions = Questions.ToList()};
                        AlternateStreamUtils.WriteData(destFileName, "data", JsonConvert.SerializeObject(surveyData));
                        CloseCommand.Execute(null);
                    }
                    catch (Exception e) {
                        MessageQueue.Enqueue(e.Message);
                    }
                }
            });

            Questions.CollectionChanged += (sender, args) => QuestionsCount = Questions.Count;
        }

        [DoNotCheckEquality]
        public string Email {
            get => _email;
            set {
                _email = value;
                if (!EmailRegex.IsMatch(value))
                    Validator.SetErrors(nameof(Email), "Email не правилен");
                else
                    Validator.ClearErrors(nameof(Email));
            }
        }

        public RelayCommand ContinueCommand { get; set; }

        public RelayCommand Add { get; set; }
        public RelayCommand Edit { get; set; }
        public RelayCommand Remove { get; set; }

        public ObservableCollection<string> Questions { get; set; } = new ObservableCollection<string>();

        // Dummy for Validation
        [DoNotCheckEquality]
        public int QuestionsCount {
            get => Questions.Count;
            set {
                if (value == 0) Validator.SetErrors(nameof(QuestionsCount), "Не добавлено ни одного вопроса");
                else Validator.ClearErrors(nameof(QuestionsCount));
            }
        }
        
        public SnackbarMessageQueue MessageQueue { get; set; } = new SnackbarMessageQueue(TimeSpan.FromSeconds(4)) {IgnoreDuplicate = false};
    }
}