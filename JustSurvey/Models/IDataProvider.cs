﻿namespace JustSurvey.Models {
    public interface IDataProvider<out T> where T : class {
        T? Provide();
    }
}