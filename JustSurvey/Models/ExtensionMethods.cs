﻿using System.Windows;

namespace JustSurvey.Models {
    public static class ExtensionMethods {
        public static Visibility ToVisibility(this bool value, bool falseIsHidden = false) {
            return value      ? Visibility.Visible :
                falseIsHidden ? Visibility.Collapsed : Visibility.Hidden;
        }
    }
}