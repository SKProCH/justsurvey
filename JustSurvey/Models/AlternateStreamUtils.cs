﻿using System;
using CodeFluent.Runtime.BinaryServices;

namespace JustSurvey.Models {
    public class AlternateStreamUtils {
        public static string? ReadData(string file, string streamName) {
            try {
                var path = $"{file}:{streamName}";
                if (NtfsAlternateStream.Exists(path))
                    return NtfsAlternateStream.ReadAllText(path);
            }
            catch (Exception) {
                // ignored
            }

            return null;
        }
        
        public static void WriteData(string file, string streamName, string content) {
            var path = $"{file}:{streamName}";
            NtfsAlternateStream.WriteAllText(path, content);
        }
    }
}