﻿using System;
using Newtonsoft.Json;

namespace JustSurvey.Models {
    public class SurveyDataProvider : IDataProvider<SurveyData> {
        public SurveyData? Provide() {
            var dataRaw = AlternateStreamUtils.ReadData(Environment.GetCommandLineArgs()[0], "data");
            if (dataRaw != null) {
                try {
                    return JsonConvert.DeserializeObject<SurveyData>(dataRaw);
                }
                catch (Exception) {
                    // ignored
                }
            }
            
            return null;
        }
    }
}