﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using JustSurvey.Annotations;

namespace JustSurvey.Models {
    public class SurveyData {
        public SurveyData() { }
        public SurveyData(string targetEmail) {
            TargetEmail = targetEmail;
        }
        
        public string TargetEmail { get; set; } = null!;
        public List<string> Questions { get; set; } = new List<string>();
    }

    public class AnswerData : INotifyPropertyChanged {
        public AnswerData(string question, string answer = "") {
            Question = question;
            Answer = answer;
        }

        public string Answer { get; set; }
        public string Question { get; set; }
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}