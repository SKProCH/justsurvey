﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace JustSurvey.Models {
    public class EmailSenderService {
        public async Task Send(string text, string reciever) {
            var from = new MailAddress(EmailSenderServiceCredentials.SenderAddress, "Just Survey");
            var to = new MailAddress(reciever);
            var m = new MailMessage(from, to) {Subject = "Прохождение опроса", Body = text, IsBodyHtml = false};
            var smtp = new SmtpClient(EmailSenderServiceCredentials.SmtpClientHost, EmailSenderServiceCredentials.SmtpClientPort) {
                Credentials = new NetworkCredential(EmailSenderServiceCredentials.SmtpClientUsername, EmailSenderServiceCredentials.SmtpClientPassword), EnableSsl = true
            };
            await Task.Run(() => { smtp.Send(m); });
        }
    }
}