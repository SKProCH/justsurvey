﻿using System;
using JustSurvey.Models;
using JustSurvey.ViewModels;
using Microsoft.Win32;
using MugenMvvmToolkit;
using MugenMvvmToolkit.Binding;
using MugenMvvmToolkit.Interfaces;
using MugenMvvmToolkit.Models;
using MugenMvvmToolkit.ViewModels;

namespace JustSurvey {
    public class JustSurveyApplication : MvvmApplication {
        public override Type GetStartViewModelType() {
            return typeof(MainVm);
        }

        private bool StartWithSurvey() {
            var dataProvider = IocContainer?.Get<IDataProvider<SurveyData>>();
            var surveyData = dataProvider?.Provide();
            return surveyData != null && Registry.CurrentUser.CreateSubKey("SOFTWARE").CreateSubKey("JustSurvey").GetValue(surveyData.TargetEmail) == null;
        }

        protected override async void StartInternal() {
            BindingServiceProvider.ResourceResolver.AddType(nameof(ExtensionMethods), typeof(ExtensionMethods));
            var viewModelProvider = IocContainer.Get<IViewModelProvider>();
            if (StartWithSurvey()) {
                var surveyContext = new DataContext(Context);
                await viewModelProvider.GetViewModel(typeof(SurveyVm), new DataContext(surveyContext))
                                       .ShowAsync((model, result) => model.Dispose(), context: surveyContext);
            }

            var context = new DataContext(Context);
            await viewModelProvider.GetViewModel(typeof(MainVm), context).ShowAsync((model, result) => model.Dispose(), context: context);
            Environment.Exit(0);
        }
    }
}