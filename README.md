# Just Survey
> Application for creating and passing small surveys  

This is my text project to understand how the Mugen MVVM ToolKit and DI containers work
## Features
- Compiled to single exe
- Can store data (survey questions and target email) in exe
- You can create new surveys directly from an already running application and save as exe
## How to compile
> I hope no one has to do this, but just in case
1. Clone this repo
2. Open solution
3. Create file `JustSurvey/Models/EmailSenderServiceCredentials.cs`
4. Write the following code in it:
    ```c#
    namespace JustSurvey.Models {
        public static class EmailSenderServiceCredentials {
            public static string SenderAddress { get; set; } = "";
            public static string SmtpClientHost { get; set; } = "";
            public static int SmtpClientPort { get; set; } = 25;
            public static string SmtpClientUsername { get; set; } = "";
            public static string SmtpClientPassword { get; set; } = "";
        }
    }
    ```
5. Correct the data to your
6. Profit